var jsonDiff = require('json-diff').diffString
var rusDiff = require('rus-diff').rusDiff
var Diff = require('diff').s

var obj1 = {
    "type":"team",
    "description":"Good",
    "trophies":[
        {"ucl":"10"}, 
        {"copa":"5"}
    ]};
var obj2 = {
    "type":"team",
    "description":"Bad",
    "trophies":[
        {"ucl":"3"}
    ]};

// const compare = jsonDiff.diffString(madrid, barca)

// console.log(compare)
function Diff(obj1, obj2) {
    const result = {};
    for(var o1 in obj1){
        if(obj2[o1]){
            result[o1] = obj1[o1] - obj2[o1];
        }
    }
    return result;
    }   
    
    console.log(Diff(obj1,obj2))

// console.log(rusDiff(madrid, barca))