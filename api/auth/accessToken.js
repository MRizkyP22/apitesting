const supertest = require('supertest');
const env = require('dotenv').config();
const api = supertest(process.env.BASE_URL_DEV);

let getToken;

const token = async(...callbacks) => {
    let output;
    if (!getToken) {
        await api.post('/users-management/v2/auth/login')
        .set('Authorization', 'Bearer '+ token)
        .send({
            username:username, 
            password:password
        })
        .then(async (response) => {
            getToken = response.body.data.accessToken;
            // console.log('response login: ', response.body)
        })
    }
    for (let index = 0; index < callbacks.length; index++) {
        const callback = callbacks[index];
        const responseData = await callback.cbFunc(getToken, ...callback.otherParams)
        // console.log(`response cb ${(index+1)}: `, responseData)
        if (index == callbacks.length - 1) {
            output = responseData;
            // console.log('output: ',output)
        }
    }
    return output
};

module.exports = {
    token
}