const supertest = require('supertest');
const env = require('dotenv').config();
const api = supertest(process.env.BASE_URL_DEV);

const data = {
    "clientId" : "dGVsa29tZGlnaXRhbHNvbHV0aW9uZDN2",
	"clientSecret" : "Njk1ODQ5ZGUtOWZhYS00YmMzLTlmYjMtMDY2MDM0NDM1YzU5Cg=="
}

let getToken;

const jwtToken = async(...callbacks) => {
    let output;
    if (!getToken) {
        await api.post('/users-management/v2/auth/generatetoken')
        .send(data)
        .then(async (response) => {
            getToken = response.body.data.accessToken;
            //console.log('response login: ', response.body)
        })
    }
    for (let index = 0; index < callbacks.length; index++) {
        const callback = callbacks[index];
        const responseData = await callback.cbFunc(getToken, ...callback.otherParams)
        //console.log(`response cb ${(index+1)}: `, responseData)
        if (index == callbacks.length - 1) {
            output = responseData;
            // console.log('output: ',output)
        }
    }
    return output
};

module.exports = {
    jwtToken
}