const supertest = require('supertest');
const env = require('dotenv').config();
const api = supertest(process.env.BASE_URL_DEV);
const page = require('../auth/jwtAuth.js');

const getLoginCallback = (token,username,password) => {
    
    const url = `/users-management/v2/auth/login`
    return api.post(url)
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + token)
        .send({
            username:username, 
            password:password
        })
}

const getLogin = async(username,password) => {
    const cb1 = {
        otherParams: [username,password],
        cbFunc: getLoginCallback
    }
    const result = await page.jwtToken(cb1)
    return result
}

module.exports = {
    getLogin
}