const supertest = require('supertest');
const env = require('dotenv').config();
const api = supertest(process.env.BASE_URL_STAGE);
const page = require('../auth/accessToken.js');

const getNipnasCallback = (token, param) => {
    
    const url = `/users/user/v1/am-nipnas/${param}`
    return api.get(url)
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + token)
}

const getNipnas = async(nik) => {
    const cb1 = {
        otherParams: [nik],
        cbFunc: getNipnasCallback
    }
    const result = await page.token(cb1)
    return result
}

const getInvalidKey = () => { 

    const url = `/users/user/v1/am-nipnas/31118`
    return api.get(url)
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + 'token')
}

module.exports = {
    getNipnas,
    getInvalidKey
}