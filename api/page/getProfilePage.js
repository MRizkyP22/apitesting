const supertest = require('supertest');
const env = require('dotenv').config();
const api = supertest(process.env.BASE_URL_STAGE);
const page = require('../auth/accessToken.js');

const getProfileCallback = (token) => { 
    
    const url = '/users-management/v2/profile'
    return api.get(url)
    .set('Content-Type', 'application/json')
    .set('Authorization', 'bearer ' + token)
}
const getProfile = () => {
    const cb1 = {
        otherParams: [],
        cbFunc: getProfileCallback
    }
    return page.token(cb1)
}

const getInvalidKey = () => { 

    const url = '/users-management/v2/profile'
    return api.get(url)
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + 'token')
}
module.exports = {
    getProfile,
    getInvalidKey
}