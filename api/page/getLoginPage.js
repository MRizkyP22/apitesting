const supertest = require('supertest');
const env = require('dotenv').config();

const api = supertest(process.env.BASE_URL_STAGE)
const basicAuth = 'Basic dGVsa29tZGlnaXRhbHNvbHV0aW9uc3Q0ZzpjNTViMDNhZS0zYWUzLTQ5MDEtOWRiZC1kMjhmYmFiZGMwMTY='

const getLogin = (username,password) => {
    
    const url = '/users-management/v2/auth/login'
    return api.post(url)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .set('Authorization', basicAuth)
    .send({
        username:username, 
        password:password
    })
}

module.exports={
    getLogin
}