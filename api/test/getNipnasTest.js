// const expect = require('chai').expect;
// const should = require('chai').should();
// const page = require('../page/getNipnasPage.js');

// const testCase = {
//    "positive" : {
//       "getList" : "As a User, I want to be able to get Nipnas User",
//    },
//    "negative" : {
//       "wrongNik" : "As a User, I cant see Nipnas when NIK is wrong",
//       "invalidApiKey" : "As a User, I should got error 401 when I send request with invalid API Key"
//    }
// }

// describe(`API GET NIPNAS`, () => {

//    const nik = '31118'
//    const nikWrong = '31119'

//    it(`@get ${testCase.positive.getList}`, async() => {
//       const response = await page.getNipnas(nik);
//       expect(response.status).to.equal(200);
//       (response.body.data[0]).should.have.property('_id');
//       (response.body.data[0]).should.have.property('nipnas');
//       (response.body.data[0]).should.have.property('corporateClientName');
//       (response.body.data[0]).should.have.property('segment');
//       (response.body.data[0]).should.have.property('location');
//       (response.body.data[0]).should.have.property('witel');
//       (response.body.data[0]).should.have.property('divre');
//    })

//    it(`@get ${testCase.negative.wrongNik}`, async() => {
//       const response = await page.getNipnas(nikWrong);
//       expect(response.status).to.equal(400);      
//       expect(response.body.message).to.equal('that is not your NIK');
//    })

//    it(`@get ${testCase.negative.invalidApiKey}`, async() => {
//       const response = await page.getInvalidKey();
//       expect(response.status).to.equal(401);
//       expect(response.body.message).to.equal('Access Token is not valid!');
//    })
// })