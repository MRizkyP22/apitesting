// const expect = require('chai').expect;
// const assert = require('chai').assert;
// const should = require('chai').should();
// const page = require('../page/getLoginPage.js');

// const testCase = {
//     "positive" : {
//         "OK": "As a User, I want to able to login"
//     },
//     "Negative": {
//         "wrongNik": "As a User, I cant login when my NIK is wrong",
//         "wrongPass": "As a User, I cant login when my pass is wrong",
//         "passMin": "As a User, I cant login when my pass is less than 6 chars"
//     }
// }

// describe ('API LOGIN EOS DGS', ()=>{
//     const username = '31118';
//     const password = '31118tds';
//     const wrongNik = '311118';
//     const wrongPass = '31118sdt';
//     const passMin = '31118'

//     it(`@POST ${testCase.positive.OK}`, async()=>{
//         const response = await page.getLogin(username,password);
//         expect(response.status).to.equal(200);
//         (response.body.data).should.have.property('accessToken');
//         (response.body.data).should.have.property('refreshToken');
//         (response.body.data).should.have.property('fullName');
//         (response.body.data).should.have.property('bud');
//         (response.body.data).should.have.property('segment');
//     });

//     it(`@POST ${testCase.Negative.wrongNik}`, async()=>{
//         const response = await page.getLogin(wrongNik,password);
//         expect(response.status).to.equal(401);
//         expect(response.body.errors.detail).to.equal('Data Not Found');
//     });

//     it(`@POST ${testCase.Negative.wrongPass}`, async()=>{
//         const response = await page.getLogin(username,wrongPass);
//         expect(response.status).to.equal(401);
//         expect(response.body.errors.detail).to.equal('Invalid username or password');
//     });

//     it(`@POST ${testCase.Negative.passMin}`, async()=>{
//         const response = await page.getLogin(username,passMin);
//         expect(response.status).to.equal(400);
//         expect(response.body.errors.password).to.equal('String length must be greater than or equal to 6');
//     });
// } );