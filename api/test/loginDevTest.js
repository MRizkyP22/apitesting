const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const page = require('../page/loginDev.js');

const testCase = {
    "positive" : {
        "OK": "As a User, I want to able to login"
    },
    "Negative": {
        "wrongNik": "As a User, I cant login when my NIK is wrong",
        "wrongPass": "As a User, I cant login when my pass is wrong",
        "passMin": "As a User, I cant login when my pass is less than 6 chars"
    }
}

describe ('API LOGIN EOS DGS', ()=>{
    const username = '31118';
    const password = '31118tds';

    it(`@POST ${testCase.positive.OK}`, async()=>{
        const response = await page.getLogin(username,password);
        expect(response.status).to.equal(200);
        (response.body.data).should.have.property('accessToken');
        (response.body.data).should.have.property('refreshToken');
        (response.body.data).should.have.property('fullName');
        (response.body.data).should.have.property('bud');
        (response.body.data).should.have.property('segment');
    });

})